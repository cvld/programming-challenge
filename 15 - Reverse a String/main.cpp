#include <iostream>
#include <string>
using namespace std;

void Tail(string & str)
{
size_t s = str.size();
for(int i = 0; i < s / 2; i++)
{
	char tmp = str[i];
	str[i] = str[s-i-1];
	str[s - i-1] = tmp;
}

}

int main()
{
    cout << "Input String:" << endl;
    string str;
    cin >> str;
    Tail(str);
    cout << endl << "New String:" << endl;
    cout << str << endl;
    return 0;
}
