#include <iostream>
#include <string>
#include <algorithm>



using namespace std;
int Count(string  str)
{
	str = str;
	int count = 0;
	if( str.size() ==0 )
		return 0;
	bool in_seperator = false;
	if( str[0] == ' ') in_seperator = true;
	for(int i = 1; i < str.size(); ++i)
	{
		if( in_seperator )
		{
			if(str[i] != ' ')
			{
				in_seperator = false;
			}

		}
		else
		{
			if(str[i] == ' ')
			{
				count++;
				in_seperator = true;
			}

		}
	}
	if(!in_seperator)
		++count;

	return count;
}


int main()
{
     cout << "Input String:" << endl;
    string str;
    getline(cin,str);

    cout << Count(str);
    return 0;
}
