#include <iostream>
#include <string>
#include "ConsoleParser.h"
#include "Option.h"
#include <cstring>
#include <ctime>

using namespace std;

void ConvertDate(string Date, string trim, size_t &year, size_t & month, size_t &day, size_t & hour, size_t &minute, size_t &second)
{
	char * date_str = (char *)malloc(Date.size());
	strcpy(date_str,Date.c_str());
	char * pch = strtok (date_str,trim.c_str());
	size_t *arr [6];
	arr[0] = &year;
	arr[1] = &month;
	arr[2] = &day;
	arr[3] = &hour;
	arr[4] = &minute;
	arr[4] = &second;
	int i = 0;
	while (pch != nullptr && i < 6)
	{
		(*arr[i++]) = atoi(pch);
		pch = strtok (nullptr, trim.c_str());
	}
return;
}

int main(int argc, char * argv[])
{
string date;
string trim;
size_t year;
size_t month;
size_t day;
size_t hour;
size_t minute;
size_t second;
year = month = day = hour = minute = second = 0;

	ConsoleParser Parser= {
		new Option<string>("d", "date", &date),
		new Option<string>("t", "trim", &trim),
		new Option<size_t>("y", "year", &year),
		new Option<size_t>("m", "month", &month),
		new Option<size_t>("da", "day", &day),
		new Option<size_t>("h", "hour", &hour),
		new Option<size_t>("mi", "minute", &minute),
		new Option<size_t>("s", "second", &second)
	};
Parser.Parse(argc, argv);
	if(!date.empty() && !trim.empty())
		ConvertDate(date,trim,year,month,day,hour,minute,second);

  time_t timer;
  struct tm y2k;
  double seconds;

  y2k.tm_hour = hour + 1; y2k.tm_min = minute; y2k.tm_sec = second;
  y2k.tm_year = year - 1900; y2k.tm_mon = month; y2k.tm_mday = day + 1;

  time(&timer);  /* get current time; same as: timer = time(NULL)  */

  seconds = difftime(timer,mktime(&y2k));

  printf ("%.f seconds since %s in the current timezone \n", seconds, asctime(&y2k));



    return 0;
}
