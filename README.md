About Me
========

Hi my name is Vladimir, I am a 2nd year student of the DNURT University.

This repository has been created in order to increase my development skills in C++.

During the development process, I try to reveal all items of challenge.jpg.

I am writing using C++11 standard. Tools that I use: Code::Blocks IDE, GCC version 4.8.1, gedit - Version 3.8.3 


Обо мне
========
Меня зовут Владимир, я студент второго курса Днепропетровского Национального университета инженеров транспорта.

Этот репозиторий был создан для повышения моих навыков разработки на языке C++

Во время разработки я попыраюсь покрыть все пункты из challenge.jpg.

Сдесь я постараюсь использовать все возможности стандарта C+11. 
###Утилиты которые я использую:
- Code::Blocks - Среда разработки с открытым исходным кодом
- GCC version 4.8.1 - Компилятор от проэкта GNU
- gedit - Version 3.8.3  - Удобный блокнот с подсветкой синтаксиса.


STATUS:
========
##Fully Done stuff

- Higher/Lower | Heads/Tails
- Temperature Converter
- Calculate Age in seconds
- FizzBuzz
- Rock Paper Scissors
- 
- Love Calculator
- Password Generator 
##Need to fix and skiped(hard) 
- Encrypt/Decrypt Algorithms ( dont work correct)
- Pseudorandom Sentence generator (i want use Markov chains in this challenge)
##Skiped (stupid)
- Name Generator 
- Hangman
##Next in road


TO-DO:
======
##Known trouble in challenge:

- Encryption not work corect :(
- 
##Other stuff:
+ Need to teach about exceptions and operators in c++and some other stuff from c++11
+ Add unit tests, need more training
+ Need to automatize work with git
+ Need some plugins for work with this readme
+ Clean repository
+ 
+ Write moar code!!! 

