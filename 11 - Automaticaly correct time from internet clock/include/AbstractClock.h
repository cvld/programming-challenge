#ifndef ABSTRACTCLOCK_H
#define ABSTRACTCLOCK_H
// FIXME (fake#1#): replace this type

typedef long timeval;

class AbstractClock
{
	public:
		AbstractClock();
		virtual ~AbstractClock();
		timeval GetTime() = 0;
		void 	SetTime(timeval time) = 0;
	protected:
	private:
};

#endif // ABSTRACTCLOCK_H
