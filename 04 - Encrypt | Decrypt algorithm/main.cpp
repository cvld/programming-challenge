#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>

#include <openssl/blowfish.h>
#include "ConsoleParser.h"
#include "Option.h"
using namespace std;



static unsigned char key[] = {
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff

};
static  unsigned char ivec[] = {
    0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff
};
int main(int argc, char * argv[])
{
	string keyst;
	bool decrypt = false;
	string in, out;
	ConsoleParser Parser= {
		new Option<string>("k", "key", &keyst),
		new Option<bool>("d", "decrypt", &decrypt),
		new Option<string>("i", "input", &in),
		new Option<string>("o", "output", &out)
	};
	Parser.Parse(argc, argv);
	if( !keyst.empty())
		memcpy(key, keyst.c_str(), (keyst.size()>(16)) ? 16: keyst.size() );

	ofstream ofile(out,std::ofstream::out| std::ofstream::trunc);
	ifstream ifile(in);
	if (!ifile || !ofile)
	{
		cout<< "No input file found"<<endl;
		return 1;
	}

	    // get length of file:
    ifile.seekg (0, ifile.end);
    int length = ifile.tellg();
    ifile.seekg (0, ifile.beg);

    if(length < 10)
	{
		cout<< "Too small file"<<endl;
		return 1;
	}
	unsigned char * inbuf = new unsigned char[length];
	unsigned char * outbuf = new  unsigned char[length];

	ifile.read ((char * )inbuf,length);

    if (ifile)
      std::cout << "all characters read successfully.";
    else
      std::cout << "error: only " << ifile.gcount() << " could be read";
      std::cout << endl;
    ifile.close();

	static BF_KEY enc_key, dec_key;
    BF_set_key(&enc_key, 128, key);
	length = length & 0xFFFFFFFF8;
    int enc = BF_ENCRYPT;
    if (decrypt)
		enc = BF_DECRYPT;
	BF_cbc_encrypt(inbuf, outbuf, length, &enc_key, ivec, enc);

	ofile.write((char * )outbuf,length);

	delete[] inbuf;
	delete[] outbuf;

    return 0;
}

