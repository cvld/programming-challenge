#include <iostream>
#include <string>
#include "ConsoleParser.h"
#include "Option.h"

using namespace std;

inline float FarToCel(float val)
{
return (val - 32) * 5/9;
}

inline float CelToFar(float val)
{
return   val*9/5 + 32;
}

int main(int argc, char * argv[])
{
	float value;
	bool reverse;
	ConsoleParser Parser= {
		new Option<float>("v", "value", &value),
		new Option<bool>("r", "reverse", &reverse),

	};
	Parser.Parse(argc, argv);

	if (reverse)
	{
	cout << value << "d. F =   ";
	value = FarToCel(value);
	cout << value << "d. C"<<endl;
	}else
	{
	cout << value << "d. C =   ";
	value = CelToFar(value);
	cout << value <<  "d. F"<<endl;
	}

    return 0;
}
