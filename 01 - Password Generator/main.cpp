#include <iostream>
#include <array>
#include <vector>
#include <string>
#include "ConsoleParser.h"
#include "Option.h"

using namespace std;

#include <functional>
int help()
{


}



//using namespace ConsoleParser;


string GenerateName(bool _table[256],size_t length)
{
	char table[256];
	size_t size=0;
	for(size_t i = 0; i < 256; ++i)
	{
		if (_table[i] == true)
			{
				table[size++] = i;
			}
	}
	string out;

	for(int i = 0; i < length; ++i)
	{
		out = out+ table[rand() % size];
	}
	return out;
}
enum class NameMode{
Nothing = 0,
Upper = 1<<1,
Lower = 1<<2,
Digit = 1<<3,
Custom= 1<<8,
};

void MakeAlph(NameMode mode, bool  table[256] )
{

	if(mode == NameMode::Lower)
	for(char i ='a'; i <='z'; i++)
		(table[i])=true;

	if(mode == NameMode::Upper)
	for(char i ='A'; i <='Z'; i++)
		(table)[i]=true;

	if(mode == NameMode::Digit)
	for(char i ='0'; i <='9'; i++)
		(table)[i]=true;

}
int main(int argc, char * argv[])
{

	string alphabet;

	bool lower;
	bool upper;
	bool digit;
	//ConsoleParser::ConsoleParser(options, arraysize(options));
	ConsoleParser Parser= {
		new Option<string>("a", "alphabet", &alphabet),
		new Option<bool>("l", "lower", &lower),
		new Option<bool>("d", "digit", &digit),
		new Option<bool>("u", "upper", &upper),
	};

	Parser.Parse(argc, argv);
		cout << "Name Generator" << endl;
		static bool _table[256];
	NameMode mode;
	if( !alphabet.empty() )
		mode = NameMode::Custom;
	else
	{
			mode = NameMode::Nothing;
		if(digit)
			mode = NameMode::Digit;
		if(upper)
			mode = NameMode::Upper;
		if(lower)
			mode = NameMode::Lower;
	}

	if (mode == NameMode::Custom)
	{
		for(auto i = 0; i < alphabet.size(); i++)
			_table[(alphabet[i])]=true;
	}
	else
		MakeAlph(mode, _table);

	 cout << "Generate 10 random names" << endl;
	for(int i =0; i <=9; i++)
    cout << GenerateName(_table,i+5) << endl;
    return 0;
}
