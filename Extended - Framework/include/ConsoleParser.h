#ifndef CONSOLEPARSER_H
#define CONSOLEPARSER_H

#include "pch.h"
class AbstractOption;

typedef std::pair<std::string, size_t> OptionsPair;

class ConsoleParser
{

	public:
		ConsoleParser(std::initializer_list<AbstractOption*> options);
		virtual ~ConsoleParser();
		void Parse(int argc, char * argv[]);
	protected:
	private:
		size_t CheckOption( std::string &option);
		size_t CheckLongOption( std::string &option);
		std::vector<OptionsPair> m_OptionsTable;
		std::vector<OptionsPair> m_LongOptionsTable;
		std::vector<AbstractOption*> m_Options;
		AbstractOption *ExecutionTable;
};


#endif // CONSOLEPARSER_H
