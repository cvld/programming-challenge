#ifndef OPTION_H
#define OPTION_H

#include "pch.h"
//fwd
#include "AbstractOption.h"

typedef std::function<void(std::vector<std::string>)> OptionHandler;

//common value getter
// must process params like:
// -o 10 or --long_option=10
template<typename Value_Type>
class Option:public AbstractOption
{
public:
	Option(std::string option, std::string long_option, Value_Type *valconst ):
	AbstractOption( option,  long_option, 1),m_Value(valconst)
	{}
	virtual int Execute(bool islong, std::vector<std::string>  &values)
	{
		if(values.size() == 1 )
		{
			std::stringstream ss;
			ss<< values[0];
			ss>>*m_Value;
			return 1;
		}
	// \todo (fake#1#): add Throw
		//else throw
	}
private:
Value_Type *m_Value;
};


//bool value getter
template<>
class Option<bool>:public AbstractOption
{
public:
	Option(std::string option, std::string long_option, bool *valconst );
	virtual int Execute(bool islong, std::vector<std::string>  &values);
private:
bool *m_Value;
};


// Option Execute function when option exist
// must process values without param like:
// -o or --long_option
template<>
class Option<OptionHandler>:public AbstractOption
{
public:
	Option(std::string option, std::string long_option, OptionHandler funct, int param_count = 0);
	virtual int Execute(bool islong, std::vector<std::string>  &values);
private:
	OptionHandler m_Function;
};


#endif // OPTION_H
