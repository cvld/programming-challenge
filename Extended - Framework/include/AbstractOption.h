#ifndef ABSTRACTOPTION_H
#define ABSTRACTOPTION_H

#include "pch.h"

// Iface
class AbstractOption
{

	public:
		AbstractOption(std::string option, std::string long_option, int param_count);
		// @param isLong - true if we found long option
		// @param
		virtual int Execute(bool islong, std::vector<std::string>  &values) = 0;
		 int GetParamCount() { return m_ParamCount;}
		 std::string GetOptionName() { return m_OptionName;}
		 std::string GetOptionLongName() { return m_OptionLongName;}

	private:
		int m_ParamCount;
		std::string m_OptionName;
		std::string m_OptionLongName;
};

#endif // ABSTRACTOPTION_H
