#include "pch.h"

#include "ConsoleParser.h"
#include "AbstractOption.h"
#include <iostream>

#include <cstring>
#include <cstdlib>

ConsoleParser::ConsoleParser(std::initializer_list<AbstractOption *> options)
{
	m_Options = std::vector<AbstractOption*> (options);
	m_LongOptionsTable.reserve(m_Options.size());
	m_OptionsTable.reserve(m_Options.size());

	for(size_t i=0; i < m_Options.size(); ++i )
	{
		std::string temp = m_Options[i]->GetOptionName();
		m_OptionsTable.push_back(OptionsPair(temp, i));
		m_LongOptionsTable.push_back(OptionsPair(m_Options[i]->GetOptionLongName(), i));
	}
	std::sort(m_OptionsTable.begin(),m_OptionsTable.end(),
		[]( std::pair<std::string, size_t> i, std::pair<std::string, size_t> j){
		return i.first < j.first; }  );
	std::sort(m_LongOptionsTable.begin(),m_LongOptionsTable.end(),
		[]( std::pair<std::string, size_t> i, std::pair<std::string, size_t> j){
		return i.first < j.first; }  );

}

size_t ConsoleParser::CheckOption( std::string &option)
{
	for( size_t i = 0; i < m_OptionsTable.size(); i ++ )
		{
			if(m_OptionsTable[i].first == option)
			return m_OptionsTable[i].second;
		}
// \todo (fake#1#): Throw error if we cant find options
		// else throw
	return -1;
}

size_t ConsoleParser::CheckLongOption( std::string &option)
{
std::string tempopt = option.substr(0,option.find_first_of("="));

	for( size_t i = 0; i < m_LongOptionsTable.size(); i ++ )
		{
			if(m_LongOptionsTable[i].first == tempopt)
				return m_LongOptionsTable[i].second;
		}
		// \todo (fake#1#): Throw error if we cant find options
		// else throw
	return -1;
}

// FIXME (fake#1#): without ecceptions if we will set option that dident expect then we will execute 1st option

void ConsoleParser::Parse(int argc, char * argv[])
{
argc--;
for(int i = 1; i <= argc; i++)
	{
		if (argv[i][0] == '-')
			{//param begin
			AbstractOption * opt = nullptr;
			bool islong = false;
			if(argv[i][1] == '-')
			{// long options begin there
				std::string temp = std::string(argv[i]+2,strlen(argv[i])-2);
				opt = m_Options[CheckLongOption(temp)];
				islong = true;
			}
			else
			{ // short options
				std::string temp = std::string(argv[i]+1,strlen(argv[i])-1);
				opt = m_Options[CheckOption(temp)];
			}
			std::vector<std::string> vec;
			if(opt->GetParamCount() == -1)
			{

					for(i = i + 1;  i <= argc; i++)
						vec.push_back(std::string(argv[i], strlen(argv[i]) ) );
			}
			else if(opt->GetParamCount() >= 1)
			{
				if(!islong)
				{
					if( argc - i >=  opt->GetParamCount() )
					{
						for(size_t j = 0; j < opt->GetParamCount(); j++)
							vec.push_back(std::string(argv[i + j + 1], strlen(argv[i + j + 1]) ) );
						//i += opt->GetParamCount();
					}
				}
				else
				{//if long option get all params
					char * params = strchr(argv[i],'=')+1;
					char * pch;
					if(params == nullptr)
						{//throww
							// \todo (fake#1#): throw error

						}
						else
						{
							  pch = strtok (params,",-");
							  while (pch != nullptr)
							  {
								vec.push_back(std::string(pch, strlen(pch) ) );
								pch = strtok (nullptr, " ,.-");
							  }
						}
				}
			}

			int val = opt->Execute(islong, vec);
			if(!islong)
				i += val;

			}
	}
}

ConsoleParser::~ConsoleParser()
{
	//dtor
}

