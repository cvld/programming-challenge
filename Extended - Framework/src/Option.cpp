#include "pch.h"

#include "Option.h"




Option<bool>::Option(std::string option, std::string long_option, bool *valconst ):
	AbstractOption( option,  long_option, 1),m_Value(valconst)
{
}

int Option<bool>::Execute(bool islong, std::vector<std::string>  &values)
{
	if(values.size() == 1 )
	{
		if(!islong)
		{
			*m_Value = !(* m_Value);
			return 0;
		}
		else
		{
			std::string tmpv = values[0] ;
			std::transform(tmpv.begin(), tmpv.end(), tmpv.begin(), ::tolower);
			if(tmpv == "1" || tmpv == "true")
				*m_Value = true;
			else *m_Value = false;
		}
		return 1;
	}// \todo (fake#1#): add Throw
	//else throw
}

Option<OptionHandler>::Option(std::string option, std::string long_option, OptionHandler funct, int param_count ):
	AbstractOption( option,  long_option, param_count), m_Function(funct)
{
}

int Option<OptionHandler>::Execute(bool islong, std::vector<std::string> & values)
{
	m_Function(values);
	return 0;
}


