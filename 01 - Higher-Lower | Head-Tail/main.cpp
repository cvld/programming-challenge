#include <iostream>
#include <string>
#include "ConsoleParser.h"
#include "Option.h"
using namespace std;

char ToLower( char a)
{
	if( a >= 'A' && a <= 'Z')
		return a + 'a' - 'A';
}

char ToUpper( char a)
{
	if( a >= 'a' && a <= 'z')
		return a + 'A' - 'a' ;
}

void ToLower(string &str)
{
	for(auto &a: str)
	{
		a = ToLower(a);
	}
}

void ToUpper(string &str)
{
	for(char &a: str)
	{
		a = ToUpper(a);
	}
}

void Tail(string & str)
{
size_t s = str.size();
for(int i = 0; i < s / 2; i++)
{
	char tmp = str[i];
	str[i] = str[s-i];
	str[s - i] = tmp;
}

}

int main(int argc, char * argv[])
{
string process;
bool lower 	= false;
bool upper 	= false;
bool tail 	= false;
bool header = false;
	ConsoleParser Parser= {
		new Option<string>("s", "string", &process),
		new Option<bool>("l", "lower", &lower),
		new Option<bool>("u", "upper", &upper),
		new Option<bool>("h", "header", &header),
		new Option<bool>("t", "tail", &tail),
	};
Parser.Parse(argc, argv);
if( process.empty())
{
cout << "Error no string was founded"<< endl;
return 1;

}

if( lower )
	ToLower(process);
else if( upper )
	ToUpper(process);

if( tail )
	Tail(process);
else if( header )
	Tail(process);
cout << process <<endl;
    return 0;
}
